<?php

// /*
// |--------------------------------------------------------------------------
// | Web Routes
// |--------------------------------------------------------------------------
// |
// | Here is where you can register web routes for your application. These
// | routes are loaded by the RouteServiceProvider within a group which
// | contains the "web" middleware group. Now create something great!
// |
// */


//frontend
Route::get('/','FrontEndController@home')->name('home');

// Route::get('/contact-us','FrontEndController@contact')->name('contact');

Route::get('/about-us/board-of-directors','FrontEndController@bod')->name('team');


Route::get('/about-us/bod','FrontEndController@bod')->name('team');

Route::get('/about-us/corporate-management','FrontEndController@corporate_management')->name('corporate');

Route::get('/about-us/board-of-directors','FrontEndController@bod')->name('team');


Route::get('/document','FrontEndController@download')->name('document');
//news and event
Route::get('/news/{slug}','FrontEndController@news')->name('news');
Route::get('/news-event','FrontEndController@newsmenu')->name('event');







Route::get('/branches','FrontEndController@branch')->name('branch');

Route::group(['as'=>'download.', 'prefix'=>'download'], function(){
Route::get('/proposal-forms','FrontEndController@propose')->name('proposal');
Route::get('/financial-reports','FrontEndController@finance')->name('finance');
Route::get('/kyc-form','FrontEndController@kyc')->name('kyc');
Route::get('/claim-intimation-form','FrontEndController@claim')->name('claim');
});

Route::get('insurance/{slug}','FrontEndController@insurances')->name('insurance');

Route::get('/about-us/company-profile-nepal','FrontEndController@about')->name('about');
Route::get('/about-us/company-profile-world','FrontEndController@company_profile_world')->name('world');
Route::get('/about-us/key-person','FrontEndController@key')->name('key');
Route::get('/profile-details/{slug}','FrontEndController@profile')->name('profile');

Route::get('contact', 'FrontEndController@getContact');
Route::post('contact','FrontEndController@postContact');

//login
Auth::routes();

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');

//backend
Route::get('/home', 'HomeController@dashboard')->name('dashboard');

Route::group([ 'prefix' => 'backend', 'middleware' => 'auth' ], function () {

//slider
Route::group(['as'=>'slider.', 'prefix'=>'slider' ], function(){
Route::get('','SliderController@index')->name('index');
Route::get('create','SliderController@create')->name('create');
Route::post('','SliderController@store')->name('store');
Route::put('{slider}','SliderController@update')->name('update');
Route::get('{slider}/edit','SliderController@edit')->name('edit');
Route::delete('{slider}','SliderController@delete')->name('destroy');
});


//news
Route::group(['as'=>'news.', 'prefix'=>'news' ], function(){
Route::get('','NewsController@index')->name('index');
Route::get('create','NewsController@create')->name('create');
Route::post('','NewsController@store')->name('store');
Route::put('{new}','NewsController@update')->name('update');
Route::get('{new}/edit','NewsController@edit')->name('edit');
Route::delete('{new}','NewsController@delete')->name('destroy');
});

//team
Route::group(['as'=>'team.', 'prefix'=>'team' ], function(){
Route::get('','TeamController@index')->name('index');
Route::get('create','TeamController@create')->name('create');
Route::post('','TeamController@store')->name('store');
Route::put('{team}','TeamController@update')->name('update');
Route::get('{team}/edit','TeamController@edit')->name('edit');
Route::delete('{team}','TeamController@delete')->name('destroy');
});

//download file
    Route::group([ 'as' => 'download.', 'prefix' => 'download' ], function ()
    {
        Route::get('', 'DownloadController@index')->name('index');
        Route::get('create', 'DownloadController@create')->name('create');
        Route::post('store', 'DownloadController@store')->name('store');
        Route::put('{download}', 'DownloadController@update')->name('update');
        Route::get('{download}/edit', 'DownloadController@edit')->name('edit');
        Route::delete('{download}', 'DownloadController@destroy')->name('destroy');

    });

    //image
      Route::group([ 'as' => 'photo.', 'prefix' => 'photo' ], function ()
        {
            Route::get('', 'PhotoController@index')->name('index');
            Route::get('create', 'PhotoController@create')->name('create');
            Route::post('store', 'PhotoController@store')->name('store');
            Route::put('{photo}', 'PhotoController@update')->name('update');
            Route::get('{photo}/edit', 'PhotoController@edit')->name('edit');
            Route::delete('{photo}', 'PhotoController@delete')->name('destroy');

        });

        //pages
          Route::group([ 'as' => 'page.', 'prefix' => 'page' ], function ()
            {
                Route::get('', 'PageController@index')->name('index');
                Route::get('create', 'PageController@create')->name('create');
                Route::post('store', 'PageController@store')->name('store');
                Route::put('{page}', 'PageController@update')->name('update');
                Route::get('{page}/edit', 'PageController@edit')->name('edit');
                Route::delete('{page}', 'PageController@delete')->name('destroy');

            });

            Route::group([ 'as' => 'insurance.', 'prefix' => 'insurance' ], function ()
              {
                  Route::get('', 'InsuranceController@index')->name('index');
                  Route::get('create', 'InsuranceController@create')->name('create');
                  Route::post('store', 'InsuranceController@store')->name('store');
                  Route::put('{insurance}', 'InsuranceController@update')->name('update');
                  Route::get('{insurance}/edit', 'InsuranceController@edit')->name('edit');
                  Route::delete('{insurance}', 'InsuranceController@delete')->name('destroy');

              });
});

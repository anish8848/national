@extends('frontend.layouts.app')

@section('title','Home')

@section('content')

 @include('frontend.layouts.pop-up')

@include('frontend.layouts.slider')

@include('frontend.layouts.services')


@if(!$news->isEmpty())
@include('frontend.news.news-list')
@else

@endif
@if(!$client->isEmpty())
@include('frontend.layouts.client')


@else
@endif

@endsection

<tr>
    <td>{{++$key}}</td>
    <td>{{ str_limit($new->title,15) }}</td>
    <td>{!!str_limit($new->content,35) !!}</td>
    <td class="text-center">
        <span class="badge">{{ $new->is_published ? 'Yes' : 'No' }}</span>
</td>

<td class="text-right">
 <a href="{{route('news.edit', $new->slug)}}" class="btn btn-flat btn-primary btn-xs">
     Edit
 </a>
 <button type="button" data-url="{{ route('news.destroy', $new->slug) }}"
         class="btn btn-flat btn-primary btn-xs item-delete">
     Delete
 </button>
</td>
</tr>

@extends('backend.layouts.app')

@section('title', 'Downloads')

@section('content')
    <section>
        <div class="section-body">
            {{ Form::open(['route' =>'download.store','class'=>'form form-validate','role'=>'form', 'files'=>true, 'novalidate']) }}
            @include('backend.download.partials.form', ['header' => 'Create a file for download'])
            {{ Form::close() }}
        </div>
    </section>
@endsection

@push('scripts')
<script src="{{ asset('backend/js/libs/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script src="{{ asset('backend/js/libs/jquery-validation/dist/additional-methods.min.js') }}"></script>


@endpush

<tr>
    <td>{{++$key}}</td>
    <td>{{ str_limit($insurance->title,15) }}</td>
    <td>{!!str_limit($insurance->content,35) !!}</td>
    <td class="text-center">
        <span class="badge">{{ $insurance->is_published ? 'Yes' : 'No' }}</span>
</td>

<td class="text-right">
 <a href="{{route('insurance.edit', $insurance->slug)}}" class="btn btn-flat btn-primary btn-xs">
     Edit
 </a>
 <button type="button" data-url="{{ route('insurance.destroy', $insurance->slug) }}"
         class="btn btn-flat btn-primary btn-xs item-delete">
     Delete
 </button>
</td>
</tr>

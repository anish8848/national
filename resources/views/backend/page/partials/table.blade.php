<tr>
    <td>{{++$key}}</td>
    <td>{{ str_limit($page->title,15) }}</td>
    <td>{!!str_limit($page->content,35) !!}</td>
    <td class="text-center">
        <span class="badge">{{ $page->is_published ? 'Yes' : 'No' }}</span>
</td>

<td class="text-right">
 <a href="{{route('page.edit', $page->slug)}}" class="btn btn-flat btn-primary btn-xs">
     Edit
 </a>



@if(!in_array($page->id,['5','6','7','8']))
 <button type="button" data-url="{{ route('page.destroy', $page->slug) }}"
         class="btn btn-flat btn-primary btn-xs item-delete">

Delete
     </button>
@else

@endif

</td>
</tr>

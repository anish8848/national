<div class="row">
  <div class="col-md-12">
    @include('partials.errors')
  </div>
  <div class="col-sm-8">
    <div class="card">
      <div class="card-head">
        <header><h4>{!! $header !!}</h4></header>
        <div class="tools visible-xs">
          <a class="btn btn-default btn-ink" onclick="history.go(-1);return false;">
            <i class="md md-arrow-back"></i>
            Back
          </a>
          <input type="submit" name="draft" class="btn btn-info ink-reaction" value="Save Draft">
          <input type="submit" name="publish" class="btn btn-primary ink-reaction" value="Publish">
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group">
              <strong>Title</strong>
              {{ Form::text('title',old('title'),['class'=>'form-control','required']) }}

            </div>
          </div>
        </div>
        <!-- <div class="row">
        <div class="col-sm-12">
        <div class="form-group">
        <strong>Meta_description*</strong>
        {{ Form::textarea('meta_description',old('meta_description'),['class'=>'form-control','placeholder' => 'meta_description','rows'=> 2, 'cols'=> 40]) }}

      </div>
    </div>
  </div> -->
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group">
        <strong>Content<Strong>
          <br>
          {{ Form::textarea('content',old('content'),['required', 'id' => 'my-ckeditor']) }}

        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <label class="text-default-dark"><strong>Featured Image</strong></label>
        <br>
        <br>
        @if(isset($page) && $page->image)
        <input type="file" name="image" class="dropify" data-default-file="{{ asset($page->image->path) }}"/>
        @else
        <input type="file" name="image" class="dropify"/>
        @endif
      </div>
    </div>
  </div>
  <br>
  <br>
  <div class="card-actionbar">
    <div class="card-actionbar-row">
      <button type="reset" class="btn btn-default ink-reaction">Reset</button>
      <input type="submit" name="draft" class="btn btn-info ink-reaction" value="Save Draft">
      <input type="submit" name="publish" class="btn btn-primary ink-reaction" value="{{ isset($page) && $page->is_published ? 'Save' : 'Publish' }}">
    </div>
  </div>
</div>
</div>
</div>

@push('scripts')
<script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script>
CKEDITOR.replace( 'my-ckeditor' );
</script>
@endpush

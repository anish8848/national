<section class="news">
  <div class="container">
    <div class="section-title">
      <div class="row">
        <div class="col-md-4 col-sm-5 col-xs-12">
          <div class="title-section text-left">
            <p>News/Event</p>
            <h2>Latest news</h2>
          </div>
        </div>
        <div class="col-md-8 col-sm-7 col-xs-12 font-20">
          <!-- <p>The Insurance Corporation is the largest publicly held personal lines property and casualty insurer in America, serving more than 16 million households nationwide. </p> -->
        </div>
      </div>
    </div>
    <div class="row">
      @foreach($news as $eachdata)
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="blogList_single_post clear_fix wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
          <div class="img_holder">
            <img src="{{asset($eachdata->image->path)}}" alt="News" class="img-responsive">
          </div>
          <!-- End of .img_holder -->
          <div class="post">
            <div class="text">

              <ul class="post-bar">
          <li><i class="fa fa-user"></i>Admin</li>
          <li><i class="fa fa-calendar"></i>{{date('d F ,Y', strtotime($eachdata->created_at))}}</li>
        </ul>
              <h4><a href="{{route('news',$eachdata->slug)}}">{{$eachdata->title}}</a></h4>
              <p>{!!substr(strip_tags($eachdata->content),0,80)!!}</p>
              <!--<div class="link"><a href="{{route('news',$eachdata->slug)}}" class="tran3s">Read More...</a></div>-->

            </div>

          </div>
          <!-- End of .post -->
        </div>

      </div>
      @endforeach


    </div>
  </div>
</section>

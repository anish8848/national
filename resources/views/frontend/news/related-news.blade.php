<div class="col-md-3 col-sm-12 col-xs-12 sidebar_styleTwo">
  <div class="wrapper">


    <div class="popular_news">
      <div class="theme_inner_title">
        <h4>latest news</h4>
      </div>

      <div class="recent-posts post-thumb">
        @foreach($related_news as $data)
        <div class="post">
          <div class="post-thumb">
            <a href="#"><img src="{{asset($data->image->path)}}" alt=""></a>
          </div>
          <h4><a href="{{route('news',$data->slug)}}">{{$data->title}}</a></h4>
          <!-- <div class="post-info">by Admin </div> -->
        </div>
        @endforeach
      </div>
    </div>

    <!-- End of .sidebar_tags -->

  </div>
  <!-- End of .wrapper -->
</div>

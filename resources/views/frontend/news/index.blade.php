@extends('frontend.layouts.app')

@section('title',$new->title)

@section('content')

	<section class="news style-2 single-blog">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-sm-8 col-xs-12">
					<div class="blogList_single_post clear_fix wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
						<div class="img_holder">
							<img src="{{ asset($new->image->path) }}" alt="News" class="img-responsive">
						</div>
						<!-- End of .img_holder -->
						<div class="post">

							<div class="text">
								<ul class="post-bar">
									<li><i class="fa fa-user"></i>Admin</li>
									<li><i class="fa fa-calendar"></i>{{date('d F ,Y', strtotime($new->created_at))}}</li>
								</ul>

								<h2>{{$new->title}}</h2><br>
								<p>{!!$new->content!!}</p>


							</div>

							<div class="post-pagination">
								<div class="next-post"><a href="#" class="prev-post">Share with</a></div>

								<ul class="share-options clearfix">
									<li><a href="#"><span class="fa fa-facebook-f"></span></a></li>
									<li><a href="#"><span class="fa fa-twitter"></span></a></li>
									<li><a href="#"><span class="fa fa-dribbble"></span></a></li>
									<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
								</ul>

								<!-- <div class="prev-post"><a href="#" class="prev-post">Next Post<i class="fa fa-long-arrow-right"></i></a> </div> -->

							</div>


						</div>
						<!-- End of .post -->
					</div>

				</div>
				<!-- _______________________ SIDEBAR ____________________ -->

				@if(!$related_news->isEmpty())
					@include('frontend.news.related-news')

				@else

				@endif

			</div>

		</div>
	</section>

@endsection

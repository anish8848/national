@extends('frontend.layouts.app')

@section('title','news/event')

@section('content')
    <!-- <section class="inner-intro bg bg-fixed bg-overlay-black-70" style="background-image:url(images/background/inner-page.png);">
        <div class="container">
            <div class="row intro-title text-center">
                <div class="col-sm-12">

                </div>
                <div class="col-sm-12">
                    <ul class="page-breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><a href="blog-grid.html">News/Event</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><span></span> </li>
                    </ul>
                </div>
            </div>
        </div>
    </section> -->

    <!--=================================
     banner -->

     <section class="news">
     		<div class="container">
     			<div class="row">
            @foreach($new as $eachdata)
     				<div class="col-md-4 col-sm-6 col-xs-12">
     					<div class="blogList_single_post clear_fix wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
     						<div class="img_holder">
     							<a href="{{route('news',$eachdata->slug)}}"><img src="{{asset($eachdata->image->path)}}" alt="News" class="img-responsive"></a>
     						</div>
     						<!-- End of .img_holder -->

     						<div class="post">

     							<div class="text">
                    <ul class="post-bar">
                <li><i class="fa fa-user"></i> Admin</li>
                <li><i class="fa fa-calendar"></i>{{date('d F ,Y', strtotime($eachdata->created_at))}}</li>
              </ul>
     								<h4><a href="{{route('news',$eachdata->slug)}}">{{$eachdata->title}}</a></h4>
     								<p>{{substr(strip_tags($eachdata->content),0,70)}}</p>
     								<div class="link"><a href="{{route('news',$eachdata->slug)}}" class="tran3s">Read More...</a></div>

     							</div>

     						</div>
     						<!-- End of .post -->
     					</div>

     				</div>
@endforeach



     				<br><br>


     			</div>
     		</div>
     	</section>



     @endsection

@extends('frontend.layouts.app')

@section('title','proposal-forms')

@section('content')

<style>
.download h2{
background-color: #056608;
padding: 7px 20px;
color: #fff;
font-size: 20px;
}
</style>



	<section class="news-content section-padding single-service-page our-features in-wrapper no-container">
		<div class="container">
			<div class="row">
				<div class="col-md-4 download">
	        <h2><b>Quick Links</b></h2>
					<div class="single-sidebar-box service-link-widget">
						<ul class="catergori-list">
							<li><a href="{{route('download.finance')}}">Financial Reports</a></li>
							<li class="active"><a href="{{route('download.proposal')}}">Purposal Forms</a></li>
							<li><a href="{{route('download.claim')}}">Claim Intimation Form</a></li>
							<li><a href="{{route('download.kyc')}}">Kyc Form</a></li>




					</div>
				</div>
        	<!-- <h5 class="uppercase">Our Brochures</h5> -->

				<div class="col-md-8">
					<div class="single-sidebar-box service-link-widget">
						  @foreach($proposal_forms as $data)
						<div class="brochures">
							<!-- <br> -->
							<ul class="brochures-lists">
								<li class="active">
				<a href="{{URL::asset("/downloads/$data->path")}}" target="_blank">
										<span>{{$data->extension}}</span>{{$data->title}}<i class="fa fa-download"></i>
									</a>
								</li>
							</ul>
						</div>
						@endforeach
					</div>
				</div>



			</div>
		</div>
	</section>


@endsection

@extends('frontend.layouts.app')

@section('title','financial-report')

@section('content')

<style>
.download h2{
background-color: #056608;
padding: 7px 20px;
color: #fff;
font-size: 20px;
}
.brochures ul>li:before{
    content: "\f0da";
    font-family: 'FontAwesome';
    margin: 0 2px;
    left: 0;
    position: absolute;
    color: #fe0000;
    font-weight: 700;

  }
</style>



	<section class="news-content section-padding single-service-page our-features in-wrapper no-container">
		<div class="container">
			<div class="row">
				<div class="col-md-4 download">
	        <h2><b>Quick Links</b></h2>
					<div class="single-sidebar-box service-link-widget">
						<ul class="catergori-list">
							<li class="active"><a href="{{route('download.finance')}}">Financial Reports</a></li>
							<li><a href="{{route('download.proposal')}}">Purposal Forms</a></li>
							<li><a href="{{route('download.claim')}}">Claim Intimation Form</a></li>
							<li><a href="{{route('download.kyc')}}">Kyc Form</a></li>




					</div>
				</div>
        	<!-- <h5 class="uppercase">Our Brochures</h5> -->

				<div class="col-md-8">
					 @foreach($financial_report as $data)
					<div class="single-sidebar-box service-link-widget">
										<!-- <br> -->
						<div class="brochures">
							<ul class="brochures-lists">
								<li class="active">
				<a href="{{URL::asset("/downloads/$data->path")}}" target="_blank">
										<span>{{$data->extension}}</span>{{$data->title}}<i class="fa fa-download"></i>
									</a>
								</li>
							</ul>

						</div>

					</div>
						@endforeach
				</div>



			</div>
		</div>
	</section>


@endsection

	<section class="our-services">
		<div class="container">
			<div class="section-title">
				<div class="row">
					<div class="col-md-4 col-sm-5 col-xs-12">
						<div class="title-section text-left">
							<p>Our Products</p>
							<h2>Insurance Coverage</h2>
						</div>
					</div>
					<!-- <div class="col-md-8 col-sm-7 col-xs-12 font-20">
						<p>When you truly love your family, your assets or your business, you protect them as best you can.We operate globally and locally, according to our customers’ needs.</p>
					</div> -->
				</div>
			</div>
			<div class="row">
				@foreach($service as $data)
				<div class="col-md-4 col-sm-6">
					<div class="single-our-service">
						<figure class="img-box">
							<a href="{{route('insurance',$data->slug)}}"><img src="{{asset($data->image->path)}}" alt="Awesome Image" /></a>
						</figure>
						<a href="{{route('insurance',$data->slug)}}">
							<h4>{{$data->title}}</h4>
						</a>
						<p>{{$data->meta_description}}</p>
						<br>
						  <div class="link"><a href="{{route('insurance',$data->slug)}}" style="color:#1b7dbd;" class="tran3s">Read More...</a></div>
					</div>

				</div>

				@endforeach
				<!-- <div class="col-md-4 col-sm-6">
					<div class="single-our-service">
						<figure class="img-box">
							<a href="#"><img src="{{asset('images/medical.jpg')}}" alt="Awesome Image" /></a>
						</figure>
						<a href="#">
							<h4>Health Insurance Policy</h4>
						</a>
						<p>Protect your home inside and out with a homeowners insurance policy from Insurance.Put it in Good Hands.</p>
					</div>
				</div> -->
				<!-- <div class="col-md-4 col-sm-6">
					<div class="single-our-service">
						<figure class="img-box">
							<a href="#"><img src="{{asset('images/6.jpg')}}" alt="Awesome Image" /></a>
						</figure>
						<a href="#">
							<h4>Cattle and Crop Insurancy Policy </h4>
						</a>
						<p>Plan ahead to help protect your family. Life insurance helps provide some financial security, so there's a way to help your family cover mortgage. </p>
					</div>
				</div> -->
				<div class="col-md-12">
					<div class="separet"></div>
				</div>
				<!-- <div class="col-md-4 col-sm-6">
					<div class="single-our-service">
						<figure class="img-box">
							<a href="#"><img src="{{asset('images/11.jpg')}}" alt="Awesome Image" /></a>
						</figure>
						<a href="#">
							<h4>Micro Insurance Policy</h4>
						</a>
						<p>Whether you're enjoying a long summer time ride, or have your bike stored away for the winter, an Insurance motorcycle insurance policy can help protect you and your motorcycle.</p>
					</div>
				</div> -->
				<!-- <div class="col-md-4 col-sm-6">
					<div class="single-our-service">
						<figure class="img-box">
							<a href="#"><img src="{{asset('images/12.jpg')}}" alt="Awesome Image" /></a>
						</figure>
						<a href="#">
							<h4>Overseas Medical Insurance Policy</h4>
						</a>
						<p>Even if you're not required to have boat insurance, it's important to protect your boat and your crew from harm. You can get that dependable coverage from Insurance at a surprisingly affordable price.</p>
					</div>
				</div> -->
				<!-- <div class="col-md-4 col-sm-6">
					<div class="single-our-service">
						<figure class="img-box">
							<a href="#"><img src="{{asset('images/air.jpg')}}" alt="Awesome Image" /></a>
						</figure>
						<a href="#">
							<h4>Aviation Insurance</h4>
						</a>
						<p>As a small business owner, you face a variety of unique challenges and circumstances. Having secure, personalized business insurance can offer invaluable protection and peace of mind.</p>
					</div>
				</div> -->

			</div>
		</div>
	</section>

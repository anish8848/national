<section class="news">
  <div class="container">
    <div class="section-title">
      <div class="row">
        <div class="col-md-4 col-sm-5 col-xs-12">
          <div class="title-section text-left">
            <p>News/Event</p>
            <h2>Latest news</h2>
          </div>
        </div>
        <div class="col-md-8 col-sm-7 col-xs-12 font-20">
          <p>The Insurance Corporation is the largest publicly held personal lines property and casualty insurer in America, serving more than 16 million households nationwide.</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="blogList_single_post clear_fix wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
          <div class="img_holder">
            <img src="images/blog/blog1.jpg" alt="News" class="img-responsive">
          </div>
          <!-- End of .img_holder -->
          <div class="post">
            <div class="text">

              <ul class="post-bar">
                <li><i class="fa fa-user"></i> Admin</li>
                <li><i class="fa fa-calendar"></i>27Feb,2019</li>
              </ul>
              <h4><a href="blog-details.html">Life Insurance for the Stages of Your Life</a></h4>
              <p>Plan ahead to help protect your family. Life insurance helps provide some financial security, so there's a way to help your family.</p>
              <div class="link"><a href="#" class="tran3s">Read More...</a></div>

            </div>

          </div>
          <!-- End of .post -->
        </div>

      </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="blogList_single_post clear_fix wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
          <div class="img_holder">
            <img src="images/blog/blog2.jpg" alt="News" class="img-responsive">
          </div>
          <!-- End of .img_holder -->
          <div class="post">

            <div class="text">
              <ul class="post-bar">
                <li><i class="fa fa-user"></i> Admin</li>
                <li><i class="fa fa-calendar"></i>27Feb,2019</li>
              </ul>

              <h4><a href="#">Matching Life Insurance to Your Needs</a></h4>
              <p>Plan ahead to help protect your family. Life insurance helps provide some financial security, so there's a way to help your family.</p>
              <div class="link"><a href="#" class="tran3s">Read More...</a></div>

            </div>

          </div>
          <!-- End of .post -->
        </div>

      </div>
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="blogList_single_post clear_fix wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
          <div class="img_holder">
            <img src="images/blog/blog3.jpg" alt="News" class="img-responsive">
          </div>
          <!-- End of .img_holder -->
          <div class="post">
            <div class="news-date">
              September 20, 2017
            </div>
            <div class="text">
              <p class="author"><a href="#">by John Terry</a></p>
              <h4><a href="#">Understanding the types of life insurance</a></h4>
              <p>Plan ahead to help protect your family. Life insurance helps provide some financial security, so there's a way to help your family.</p>
              <div class="link"><a href="#" class="tran3s">Read More...</a></div>

            </div>

          </div>
          <!-- End of .post -->
        </div>

      </div>

    </div>
  </div>
</section>

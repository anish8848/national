
	<!-- Start header top -->
	<header class="main-header">

		<div class="header-upper">
			<div class="container">
				<div class="clearfix">

					<div class="pull-left logo-outer">
						<div class="logo">
            <a href="{{route('home')}}"><img src="{{asset('images/logo.jpg')}}" alt="Nepal Insurance" title="Nepal Insurance Company"></a>
						</div>
					</div>

					<div class="pull-right upper-right clearfix">

						<!--Info  Box-->
						<div class="upper-column info-box">
							<div class="icon-box"><span class="icon-location-pin"></span></div>
							Address : <span class="light-text">KKM Building (2nd Floor), P.O Box: 376,  <br>Tripureshwor, Kathmandu, Nepal</span>
						</div>

						<!--Info Box-->

						<div class="upper-column info-box">
							<div class="icon-box"><span class="icon-geography"></span></div>
							Call Us : <span class="light-text">0977-1-4260366 / 4250710 / 4254045</span> <br>Mail Us : <span class="light-text">info@nicnepal.com.np</span>
						</div>

						<!--Info Box-->
						<div class="upper-column info-box">
							<div class="icon-box"><span class="icon-chronometer-outline"></span></div>
							Office Time : <span class="light-text"><br>Sunday-Friday 10:00 - 17:00</span>
						</div>
					</div>

				</div>
			</div>
		</div>
	</header>
	<!-- End header top -->

	<!-- Start nav menu -->
	<section class="mainmenu-wrapper stricky">
		<div class="container">
			<nav class="mainmenu-holder pull-left">
				<div class="nav-header">
					<ul class="navigation">
						<li class="dropdown">
							<a href="{{route('about')}}">About</a>
							<ul class="submenu">
								<li><a href="{{route('about')}}">Company Profile(Nepal)</a></li>
								<li><a href="{{route('world')}}">Company Profile(World)</a></li>
								<li><a href="{{route('key')}}">Key Person</a></li>
								<li><a href="{{route('team')}}">Board of Director</a></li>
									<li><a href="{{route('corporate')}}">Corporate Management</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#">Insurance</a>
							<ul class="submenu">
								@php $insurances = \App\Insurance::get() @endphp
			@if($insurances != '')
			@foreach($insurances as $insurance)
								<li><a href="{{route('insurance',$insurance->slug)}}">{{$insurance->title}}</a></li>
							@endforeach
							@endif
							</ul>
						</li>
						<li>
							<a href="{{route('event')}}">News/Event</a>

						</li>
						<li>
							<a href="#">Learning Zone</a>

						</li>
						<li>
							<a href="{{route('branch')}}">Branch</a>

						</li>
						<li class="dropdown">
							<a href="#">Downloads</a>
							<ul class="submenu">
								<li><a href="{{route('download.finance')}}">Financial Report</a></li>
								<li><a href="{{route('download.proposal')}}">Proposal Form</a></li>
								<li><a href="{{route('download.claim')}}">Claim Intimation Form</a></li>
								<li><a href="{{route('download.kyc')}}">Kyc Form</a></li>
							</ul>
						</li>
						<li><a href="{{url('contact')}}">Contact</a></li>
						<li><a href="{{route('home')}}"><img src="{{asset('images/nepal_flag.png')}}" alt="national insurance" height="25px" width="25px" style="margin-bottom:-20px;"></a></li>
					</ul>
				</div>
				<div class="nav-footer hidden-lg">
					<ul>
						<li><button class="menu-expander"><i class="fa fa-list-ul"></i></button></li>
					</ul>
				</div>
			</nav>

			<div class="search-box pull-right">
				<form action="#" class="clearfix">
					<input type="text" placeholder="Search">
					<button><i class="fa fa-search"></i></button>
				</form>

			</div>
		</div>
	</section>
	<!-- End nav menu -->


<!-- Footer top -->
<footer class="footer has-black-color-overlay has-dark-texture">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-6">
        <div class="footer-widget about-widget">
          <a href="#"><img src="{{asset('images/logo.jpg')}}" alt="" /></a>
          <p>National Insurance Company is one of the leading public sector non life insurance companies of India, Headquartered in Kolkata, NIC’s foreign operation is carried out through its branch office in Nepal.</p>
          <ul class="list-inline social">
            <li>
              <a href="#"><i class="fa fa-facebook"></i></a>
            </li>
            <!-- comment for inline hack
                  -->
            <li>
              <a href="#"><i class="fa fa-twitter"></i></a>
            </li>

            <li>
              <a href="#"><i class="fa fa-instagram"></i></a>
            </li>

            <li>
              <a href="#"><i class="fa fa-linkedin"></i></a>
            </li>
          </ul>
        </div>
        <!-- /.footer-widget about-widget -->
      </div>
      <!-- /.col-md-3 -->


      <!-- /.col-md-3 -->
      <div class="col-md-3 col-sm-6">
        <div class="footer-widget link-widget">
          <div class="inner-title">
            <h3><span class="white-color-text">Our Product</span></h3>
            <span class="decor-line"></span>
          </div>
          <!-- /.sec-title-two -->
          <ul class="links">
            @php $insurances = \App\Insurance::get() @endphp
  @if($insurances != '')
  @foreach($insurances as $insurance)
            <li><a href="{{route('insurance',$insurance->slug)}}">{{$insurance->title}}</a></li>

            @endforeach
            @endif

          </ul>
          <!-- /.links -->
        </div>
        <!-- /.footer-widget link-widget -->
      </div>
      <!-- /.col-md-3 -->

      <!--<div class="col-md-3 col-sm-6">-->
      <!--  <div class="footer-widget link-widget">-->
      <!--    <div class="inner-title">-->
      <!--      <h3><span class="white-color-text">Tools&Links</span></h3>-->
      <!--      <span class="decor-line"></span>-->
      <!--    </div>-->
          <!-- /.sec-title-two -->
      <!--    <ul class="links">-->
      <!--      <li><a href="#">Agent</a></li>-->
      <!--      <li><a href="#">Tenders</a></li>-->
      <!--      <li><a href="#">Right to Information</a></li>-->
      <!--      <li><a href="#">Surveyors</a></li>-->
      <!--      <li><a href="#">Recruitment</a></li>-->
      <!--      <li><a href="#">Grievance</a></li>-->
      <!--      <li><a href="#">Vendors</a></li>-->

      <!--    </ul>-->
          <!-- /.links -->
      <!--  </div>-->
        <!-- /.footer-widget link-widget -->
      <!--</div>-->

      <!-- <div class="col-md-3 col-sm-6">
        <div class="footer-widget link-widget">
          <div class="inner-title">
            <h3><span class="white-color-text">Tools&Links</span></h3>
            <span class="decor-line"></span>
          </div>

          <ul class="links">
            <li><a href="#">Agent</a></li>
            <li><a href="#">Tenders</a></li>
            <li><a href="#">Right to Information</a></li>
            <li><a href="#">Surveyors</a></li>
            <li><a href="#">Recruitment</a></li>
            <li><a href="#">Grievance</a></li>
            <li><a href="#">Vendors</a></li>

          </ul>

        </div>

      </div> -->

      <!-- /.col-md-3 -->
      <div class="col-md-3 col-sm-6">
        <div class="footer-widget link-widget">
          <div class="inner-title">
            <h3><span class="white-color-text">Quick Links</span></h3>
            <span class="decor-line"></span>
          </div>
          <!-- /.sec-title-two -->
          <ul class="links">
            <li><a href="#">News/Event</a></li>
            <li><a href="#">Learning Zone</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Terms and Conditions</a></li>
            <li><a href="#">Branch</a></li>
            <li><a href="#">See Map</a></li>
          </ul>
          <!-- /.links -->
        </div>
        <!-- /.footer-widget link-widget -->
      </div>
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container -->
</footer>
<!-- End Footer top -->
<section class="footer-bottom">
  <div class="container">

<div class="center">
  <div class="col-xs-12 text-center">
    <p><b style="color:#1d1e22;">&copy;&nbsp;&nbsp;2019&nbsp;&nbsp;</b><a href="{{route('home')}}" style="color:#008b92"><b> National Insurance</b></a><b style="color:#1d1e22;">&nbsp;&nbsp;&reg;</b></p>
     </div>
  </div>
  </div>
  <!-- /.container -->
</section>

<style>
.text-center>p>a>b:hover{
  color:#333333;
}
</style>


<!-- Start of LiveChat (www.livechatinc.com) code -->
<!-- <script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 10813127;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<noscript>
<a href="https://www.livechatinc.com/chat-with/10813127/" rel="nofollow">Chat with u</a>,
powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a>
</noscript> -->
<!-- End of LiveChat code -->

<!--Start of Tawk.to Script-->

<!--Start of Tawk.to Script-->
<!-- <script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5cb5a4b7d6e05b735b42d199/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script> -->
<!--End of Tawk.to Script-->

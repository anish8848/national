@extends('frontend.layouts.app')

@section('title','aout-us')

@section('content')

<!--<section class="inner-intro bg bg-fixed bg-overlay-black-70" style="background:url(images/background/inner-page.png);">-->
<!--    <div class="container">-->
<!--        <div class="row intro-title text-center">-->
<!--            <div class="col-sm-12">-->

<!--            </div>-->
<!--            <div class="col-sm-12">-->
<!--                <ul class="page-breadcrumb">-->
<!--                    <li><a href="#"><i class="fa fa-home"></i>Home</a> <i class="fa fa-angle-double-right"></i></li>-->
<!--                    <li><a href="blog-grid.html">News/Event</a> <i class="fa fa-angle-double-right"></i></li>-->
<!--                    <li><span></span> </li>-->
<!--                </ul>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<style>
.text p
  {
    font-family: sans-serif;
      font-size: 16px;
      color: #333333;
      margin-bottom: 15px;
      line-height: 150%;
      text-align: justify;
      font-weight: 300;

}
.text ul>li:before{
    content: "\f105";
    font-family: 'FontAwesome';
    margin: 0 2px;
    left: 0;
    position: absolute;
    color: #056608;
    font-weight: 700;

  }
</style>

<section class="default-column sec_padd"  style="background-color:#f4fdfd;">
  <div class="container">
    <div class="section-title">
      <div class="row">
        <div class="col-md-4 col-sm-5 col-xs-12">
          <div class="title-section text-left">
            <p>{{$company_profile->title}}</p>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <h3 align="center"><strong><u>National Insurance</u></strong></h3>
      <h4 align="center"><strong>Trusted Since 1906</strong></h4>

      </div>

      <div class="column col-md-12 col-sm-12 col-xs-12">
        <div class="text">
          <p>{!!$company_profile->content!!}</p><br>
        </div>
      </div>



    <div class="section-title">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="title-section text-left">
            <p style="font-weight:700;">{{$mission->title}}</p>
          </div>
          <div class="text">
            <p>{!!$mission->content!!}</p>
          </div>
        </div>
      </div>
    </div>


    <div class="section-title">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="title-section text-left">
            <p style="font-weight:700;">{{$vision->title}}</p>
          </div>
          <div class="text">
            <p>{!!$vision->content!!}</p>
          </div>
        </div>
      </div>
    </div>


    <div class="section-title">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="title-section text-left">
            <p style="font-weight:700;">{{$products->title}}</p>
          </div>
          <div class="text">
            <p>{!!$products->content!!}</p>
          </div>
        </div>
      </div>
    </div>

    <div class="section-title">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="title-section text-left">
            <p style="font-weight:700;">{{$awards->title}}</p>
          </div>
          <div class="text">
            <p>{!!$awards->content!!}</p>
          </div>
        </div>
      </div>
    </div>


    <div class="section-title">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="title-section text-left">
            <p style="font-weight:700;">{{$corporate->title}}</p>
          </div>
          <div class="text">
            <p>{!!$corporate->content!!}</p>
          </div>
        </div>
      </div>
    </div>

</div>
</section>







@endsection

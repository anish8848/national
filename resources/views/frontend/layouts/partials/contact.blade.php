        @extends('frontend.layouts.app')

@section('title','contact-us')

@section('content')


    <!-- <section class="inner-intro bg bg-fixed bg-overlay-black-70" style="background-image:url(images/background/inner.jpg);">
        <div class="container">
            <div class="row intro-title text-center">
                <div class="col-sm-12">
                    <div class="section-title"><h1 class="title text-white">Contact</h1></div>
                </div>
                <div class="col-sm-12">
                    <ul class="page-breadcrumb">
                        <li><a href="index.html"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><a href="contact.html">pages</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><span>Contact</span> </li>
                    </ul>
                </div>
            </div>
        </div>
    </section> -->

    <!--=================================
     banner -->



	<section class="contact-post contact-body">
		<div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="single-contact-information email">
                        <div class="heading">
                            <div class="icon">
                                <i class="ion-email"></i>
                            </div>
                            <h3>Connect With Us</h3>
                        </div>
                        <div class="content">
                            <h4><i class="fa fa-phone"></i>&nbsp;0977-1-4260366 / 4250710 / 4254045 / 4254146 / 4266681</h4>
                            <h4><i class="fa fa-envelope"></i>&nbsp;&nbsp;info@nicnepal.com.np</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="single-contact-information working">
                        <div class="heading">
                            <div class="icon">
                                <i class="ion-code-working"></i>
                            </div>
                            <h3>Working Hours</h3>
                        </div>
                        <div class="content">
                            <p>
                            Sun-Thu: 10:00 to 17:00,<br>
                             &nbsp; Fri :10:00 to 14:00
                            </p>

                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="single-contact-information call">
                        <div class="heading">
                            <div class="icon">
                                <i class="ion-android-call"></i>
                            </div>
                            <h3>Find Us</h3>
                        </div>
                        <div class="content">
                            <h4><span><i class="fa fa-map-marker"></i>  KKM Building (2nd Floor),<br> P.O Box: 376, Tripureshwor, <br>Kathmandu, Nepal</span></h4>
                            <p>

                            </p>

                        </div>
                    </div>
                </div>

            </div>
			<div class="border-bottom"></div>

		</div>
	</section>

	<section class="single_contact_form">
		<div class="container">
			<div class="section-title text-center">
				<div class="row">
					<div class="col-md-12">
						<h2>Send Your Message</h2>
						<span class="decor"></span>
					</div>
				</div>
			</div>

			<div class="row">
				<!--Form Column -->
				<div class="column form-column col-md-9 col-sm-9 col-xs-12">
					<!--form-box-->
					<div class="form-box default-form">
						<div class="default-form">
							<form action="#"  method="POST">
@csrf
                <div class="row clearfix">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<input type="text" name="name" value="" placeholder="Your Name *" required>
										</div>
										<div class="form-group">
											<input type="email" name="email" value="" placeholder="Your Mail *" required>
										</div>
										<div class="form-group">
											<input type="text" name="subject" value="" placeholder="Subject" required>
										</div>

									</div>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="form-group">
											<textarea name="message"  placeholder="Your Message...." required></textarea>
										</div>
										<div class="form-group">
											<button type="submit" class="thm-btn thm-color" value="submit">send message</button>
										</div>
									</div>


								</div>
							</form>
						</div>
					</div>
				</div>


			</div>
		</div>
	</section>

<section class="home-google-map" style="display: block">
    <div class="container">
      <div class="row">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7065.52923688337!2d85.313764!3d27.693669!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x84ac787ee3f2610a!2sNational+Insurance!5e0!3m2!1sen!2snp!4v1549445031307" width="1200" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
   </div>
</section>
@endsection

@extends('frontend.layouts.app')

@section('title',$insurance->title)

@section('content')

    <!--=================================
     banner -->
<style>
.text p{
  font-family: sans-serif;
    font-size: 16px;
    color: #333333;
    margin-bottom: 15px;
    line-height: 150%;
    text-align: justify;
    font-weight: 300;
}

</style>

 <section class="news-content section-padding single-service-page our-features in-wrapper no-container">
   <div class="container">
     <div class="row">
       <div class="col-md-3">

         <div class="single-sidebar-box service-link-widget">
           <ul class="catergori-list" style="background-color:#f4fdfd;">
             @php $insurances = \App\Insurance::get() @endphp
   @if($insurances != '')
   @foreach($insurances as $ins)
             <li class="@if($ins->slug == $insurance->slug) active @endif"><a href="{{route('insurance',$ins->slug)}}">{{$ins->title}}</a></li>
           @endforeach
           @endif
           </ul>
           <br>
           <br>

           <br><br>
           @foreach($tender as $data)
           <div class="brochures">
  <h4 class="uppercase" style="color:red;">Tender Notice</h4>
             <br>
             <ul class="brochures-lists" style="list-style:square;">
               <li class="active">
                 <a href="{{URL::asset("/downloads/$data->path")}}" target="_blank">
              {{$data->title}}<i class="fa fa-download"></i>
                 </a>
               </li>

             </ul>
           </div>
           @endforeach
         </div>
       </div>
       <div class="col-md-9">
         <div class="outer">
           <div class="img-box">
                         <img src="{{asset($insurance->image->path)}}" alt="" style="box-shadow: 3px 3px 1px rgba(0,0,0,0.3);border: 1px solid #ddd;padding: 0px;width:819px;height:195px;">
           </div>
         </div>
         <br>
         <div class="outer">
           <div class="text-box">
             <div class="section-title inner">
               <h2>{{$insurance->title}}</h2>
               <span class="decor"></span>
             </div>
             <div class="text">
               <p>{!! $insurance->content !!}</p>

             </div>
           </div>
         </div>
         <br>
         <div class="mt-30"></div>

         <br>

       </div>
     </div>
   </div>
 </section>


@endsection

@extends('frontend.layouts.app')

@section('title','Corporate Management')

@section('content')


<section class="our-team">
<div class="container">
  <div class="section-title">
    <div class="row">
      <div class="col-md-4 col-sm-5 col-xs-12">
        <div class="title-section text-left">
          <p>Corporate Management</p>
        </div>
      </div>
      <div class="col-md-8 col-sm-7 col-xs-12 font-20">

      </div>
    </div>
  </div>
 <div class="row">
  @foreach($corporate as $data)
   <div class="col-md-3 col-sm-3 col-xs-6">

     <div class="single-team-member">

       <figure class="img-box">
      <img src="{{asset($data->image->path)}}" alt="" height="157px" width="125px">
       </figure>


       <h4>{{$data->name}}</h4>

         <p class="position" style="font-size:11px;">{{$data->position}}</p>
         <p style="color:#991122;font-size:14px;">{{$data->email}}</p>



     </div>

   </div>
@endforeach


 </div>

</div>
</section>


@endsection

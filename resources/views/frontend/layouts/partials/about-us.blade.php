@extends('frontend.layouts.app')

@section('title','about')

@section('content')


<style>
.text p
  {
    font-family: sans-serif;
      font-size: 16px;
      color: #333333;
      margin-bottom: 15px;
      line-height: 150%;
      text-align: justify;
      font-weight: 300;

}

</style>
  @if(empty(!$company))
<section class="about-insurance-agency sec_padd">
  <div class="container">

    <div class="section-title">
      <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="title-section text-left">
            <p>National Insurance</p>
            <h2>{{$company->title}}</h2>
          </div>
        </div>


      </div>
    </div>

    <div class="row">

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="text">
            <p>{!!$company->content!!}</p>
        </div>
        <br>
        <br>

      </div>




    </div>


  </div>
</section>
@else

@endif








@endsection

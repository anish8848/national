@extends('frontend.layouts.app')

@section('title','key-person')

@section('content')

<style>
.msn ul>li:before{
    content: "\f105";
    font-family: 'FontAwesome';
    margin: 0 2px;
    left: 0;
    position: absolute;
    color: #056608;
    font-weight: 700;

  }
  .msn p
    {
      font-family: sans-serif;
        font-size: 16px;
        color: #333333;
        margin-bottom: 15px;
        line-height: 150%;
        text-align: justify;
        font-weight: 300;

  }
</style>

	<section class="our-achivement padd-70">
		<div class="container">
			<div class="section-title">
				<div class="row">
          <div class="title-section text-left">
            <p>{{$key->title}}</p>
            <!-- <h3>CEO Profile</h3> -->
          </div>
					<div class="col-md-4 col-sm-5 col-xs-12">
						<div class="title-section text-left">
              <figure class="img-box">
    						<img src="{{asset($key->image->path)}}" alt="">
                <br>
                <h4 style="font-weight:600;padding-left:66px;">Mr. Arun Kaushal</h4>
              <hr>
                 <h5 style="font-weight:600;padding-left:66px;font-weight:14px;color:#991122;">C.E.O NICL Nepal</h5>
    					</figure>
						</div>
					</div>
					<div class="col-md-8 col-sm-7 col-xs-12 msn">
						<p>{!!$key->content!!}</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12">

				</div>

			</div>
		</div>
	</section>


@endsection

@extends('frontend.layouts.app')

@section('title','branches')

@section('content')

<style>
.b-box{
  width: 100%;
  border: 1px solid #666;
  padding: 10px;
  background-color: #f8f8f8;
  margin-top: 25px;
}
.b-box h2 {
  font-size: 15px;
}
.b-box p {
  font-size: 15px;
  margin-bottom: 5px;
}
.pqr{
  color:#056608;
  font-size:20px;
}

</style>
<section class="branches">
  <div class="container">
    <div class="row">

      <ul id="myTabs" class="nav nav-pills nav-justified" role="tablist" data-tabs="tabs">
        <li class="active"><a href="#inside" data-toggle="tab">Inside Valley</a></li>
        <li><a href="#outside" data-toggle="tab">Outside Valley</a></li>

      </ul>
      <hr>
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="inside">

          <div class="col-sm-6 col-lg-4">
            <div class="b-box">
              <h3 class="pqr"><b>Kathmandu Controlling Office</b></h3>
              <br>
              <p><b>Address :</b>&nbsp;2nd Floor, KKM Builiding,Tripureshor</p>
              <p><b>Telehone :</b>+977 1 4260366/4250710/4266681/<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4254045</p>

              <!-- <p><b>Fax No :</b>&nbsp;4215117</p> -->
              <p><b>P.O.Box :</b>&nbsp;376, Kathmandu, Nepal</p>
              <p><b>E-Mail :</b>&nbsp; <a href="#">info@nicnepal.com.np</a></p>

            </div>
          </div>

          <div class="col-sm-6 col-lg-4">
            <div class="b-box">
              <h3 class="pqr"><b>Kathmandu Branch I</b></h3>
              <br>
              <p><b>Address :</b>&nbsp;4th Floor, Citi Hub, Tinkune</p>
              <p><b>Telehone :</b>977 1 5192031</p>
              <p><b>E-Mail :</b>&nbsp; <a href="#">kb1@nicnepal.com.np</a></p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>
          </div>

          <div class="col-sm-6 col-lg-4">
            <div class="b-box">
              <h3 class="pqr"><b>Kathmandu Branch II</b></h3>
              <br>
              <p><b>Address :</b>&nbsp;2nd Floor, KKM Builiding,Tripureshor</p>
              <p><b>Telehone :</b>+977 1 4260366/4250710/4266681/<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4254045</p>

              <!-- <p><b>Fax No :</b>&nbsp;4215117</p> -->
              <p><b>P.O.Box :</b>&nbsp;376, Kathmandu, Nepal</p>
              <p><b>E-Mail :</b>&nbsp; <a href="#">kb2@nicnepal.com.np</a></p>

            </div>
          </div>
        </div>

        <div role="tabpanel" class="tab-pane fade" id="outside">
          <div class="col-sm-6 col-lg-4">
            <div class="b-box">
              <h3 class="pqr"><b>Birgunj  Branch Office</b></h3>
              <br>
              <p><b>Address :</b>&nbsp;Geeta Mandir Road,Ward No.13,<br>Birgunj,Nepal</p>
              <p><b>Telehone :</b>&nbsp;051-530621,  051-521625</p>
              <p><b>E-Mail :</b> <a href="#">nicl.brj@nicnepal.com.np</a></p>
              <p>&nbsp;</p>

            </div>
          </div>

          <div class="col-sm-6 col-lg-4">
            <div class="b-box">
              <h3 class="pqr"><b>Biratnagar  Branch Office</b></h3>
              <br>
              <p><b>Address :</b>&nbsp;Yeshoda Bhawan (3rd Floor), <br>Opp. ShivMandir,Dharan Road, Biratnagar,Nepal</p>
              <p><b>Telehone :</b>&nbsp;021-521427</p>
              <p><b>P.O.Box :&nbsp;</b>15,Nepal </p>
              <p><b>E-Mail :</b> <a href="#">nicl.brt@nicnepal.com.np</a></p>

            </div>
          </div>

          <div class="col-sm-6 col-lg-4">
            <div class="b-box">
              <h3 class="pqr"><b>Nepalgunj  Branch Office</b></h3>
              <br>
              <p><b>Address :</b>&nbsp;Dhamboji, Surkhet Road, Nepalgunj,Nepal</p>
              <p><b>Telehone :</b>&nbsp; 081- 520308 </p>
              <p><b>E-Mail :</b> <a href="mailto:gopal.kafle@icfcbank.com">nicl.nepalgunj@nicnepal.com.np</a></p>
              <p>&nbsp;</p>
            </div>
          </div>

          <div class="col-sm-6 col-lg-4">
            <div class="b-box">
              <h3 class="pqr"><b>Pokhara  Branch Office</b></h3>
              <br>
              <p><b>Address :</b>&nbsp;2nd Floor, Arbind Complex,<br>(infront of Aangan),New Road, Pokhara, Nepal.</p>
              <p><b>Telehone :</b>&nbsp;061-525114</p>
              <p><b>E-Mail :</b> <a href="#"> nicl.pokhara@nicnepal.com.np</a></p>

            </div>
          </div>

          <div class="col-sm-6 col-lg-4">
            <div class="b-box">
              <h3 class="pqr"><b>Hetauda Sub  Branch Office</b></h3>
              <br>
              <p><b>Address :</b>&nbsp;TCN Bypass road,Hetauda, Nepal</p>
              <p><b>Telehone :</b>&nbsp; 057-521236 </p>

              <p><b>E-Mail :</b> <a href="#">nicl.hetauda@nicnepal.com.np</a></p>
              <p>&nbsp;</p>
            </div>
          </div>

          <div class="col-sm-6 col-lg-4">
            <div class="b-box">
              <h3 class="pqr"><b>Janakpur Sub  Branch Office</b></h3>
              <br>
              <p><b>Address :</b>&nbsp;Bhanu Chowk, Ring Road,(Above Yamaha Motors) Janakpur, Nepal</p>
              <p><b>Telehone :</b>&nbsp; 041-420646 </p>
              <p><b>E-Mail :</b> <a href="#">nicl.janakpur@nicnepal.com.np</a></p>
              <p>&nbsp;</p>

            </div>
          </div>

          <div class="col-sm-6 col-lg-4">
            <div class="b-box">
              <h3 class="pqr"><b>Narayanghat Sub Branch Office</b></h3>
              <br>
              <p><b>Address :</b>&nbsp;2nd Floor, Charan Tower, Near Standard Chartered Bank,Lionschowk, Narayangar, Nepal</p>
              <p><b>Telehone :</b>&nbsp;056-572455</p>

              <p><b>E-Mail :</b> <a href="#">nicl.narayangarh@nicnepal.com.np</a></p>
              <p>&nbsp;</p>
            </div>
          </div>

          <div class="col-sm-6 col-lg-4">
            <div class="b-box">
              <h3 class="pqr"><b>Butwal Sub  Branch Office</b></h3>
              <br>
              <p><b>Address :</b>&nbsp;2nd Floor, Krish Building, Near Kist Bank,Milan Chowk, Butwal, Nepal</p>
              <p><b>Telehone :</b>&nbsp;071-547854</p>

              <p><b>E-Mail :</b> <a href="#">nicl.butwal@nicnepal.com.np</a></p>
              <p>&nbsp;</p>
            </div>
          </div>

          <div class="col-sm-6 col-lg-4">
            <div class="b-box">
              <h3 class="pqr"><b>Dang Sub  Branch Office</b></h3>
              <br>
              <p><b>Address :</b>&nbsp;Ratanpur Chowk, Ghorahi, Dang, Nepal</p>
              <p><b>Telehone :</b>&nbsp;082-563912</p>
              <p><b>E-Mail :</b> <a href="#">nicl.dang@nicnepal.com.np</a></p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
@endsection
<!-- <div class="col-xs-4 col-sm-4">

</div> -->

@extends('frontend.layouts.app')

@section('title','download')

@section('content')

    <section class="inner-intro bg bg-fixed bg-overlay-black-70" style="background-image:url(images/background/inner-page.png);">
        <div class="container">
            <div class="row intro-title text-center">
                <div class="col-sm-12">
                    <div class="section-title"><h1 class="title text-white">Financial Reports</h1></div>
                </div>
                <div class="col-sm-12">
                    <ul class="page-breadcrumb">
                        <li><a href="index.html"><i class="fa fa-home"></i> Home</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><a href="business-insurance.html">Download</a> <i class="fa fa-angle-double-right"></i></li>
                        <li><span>Financial Reports</span> </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!--=================================
     banner -->


	<section class="news-content section-padding single-service-page our-features in-wrapper no-container">
		<div class="container">
			<div class="row">
        	<h5 class="uppercase">Our Brochures</h5>
        @foreach($docs as $data)
				<div class="col-md-6">
					<div class="single-sidebar-box service-link-widget">
						<div class="brochures">

							<br>
							<ul class="brochures-lists">
								<li class="active">
									<a href="#">
										<span>PDF</span>{{$data->name}}<i class="fa fa-download"></i>
									</a>
								</li>
								<!-- <li>
									<a href="#">
										<span>TXT</span>Our Brouchure.txt<i class="fa fa-download"></i>
									</a>
								</li> -->
							</ul>
						</div>
					</div>
				</div>
@endforeach


			</div>
		</div>
	</section>


@endsection

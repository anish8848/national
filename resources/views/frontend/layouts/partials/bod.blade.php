@extends('frontend.layouts.app')

@section('title','Board Of Director')

@section('content')


<section class="our-team">
<div class="container">
  <div class="section-title">
    <div class="row">
      <div class="col-md-4 col-sm-5 col-xs-12">
        <div class="title-section text-left">
          <p>Board Of Directors</p>
        </div>
      </div>
      <div class="col-md-8 col-sm-7 col-xs-12 font-20">

      </div>
    </div>
  </div>
 <div class="row">
  @foreach($team as $data)
   <div class="col-md-3 col-sm-3 col-xs-6">

     <div class="single-team-member">
   <a href="{{route('profile',$data->slug)}}">
       <figure class="img-box">
      <img src="{{asset($data->image->path)}}" alt="" height="157px" width="125px">
       </figure>
</a>

       <h4 style="font-weight:400;color:#008b92;"><a href="{{route('profile',$data->slug)}}" target="_blank">{{$data->name}}</a></h4>

         <p style="font-size:11px;color:#991122;">{{$data->position}}</p>
         <!-- <p style="color:#991122;">cmd@nic.co.in</p> -->



     </div>

   </div>
@endforeach


 </div>

</div>
</section>


@endsection

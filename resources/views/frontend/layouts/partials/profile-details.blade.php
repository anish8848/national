@extends('frontend.layouts.app')

@section('title',$bod->name)

@section('content')
<style>
.text p
  {
    font-family: sans-serif;
      font-size: 16px;
      color: #333333;
      margin-bottom: 15px;
      line-height: 150%;
      text-align: justify;
      font-weight: 300;

}
</style>


    <!--=================================
     banner -->

	<section class="about-insurance-agency sec_padd">
		<div class="container">

			<div class="row">
				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
					<figure class="img-box">
						<img src="{{asset($bod->image->path)}}" alt="">
					</figure>
				</div>

				<div class="column col-md-10 col-sm-12 col-xs-12">
					<div class="section-title inner">
						<h2>{{$bod->name}}</h2>
						<span class="decor"></span><br>
            <h5 style="color:#991122;font-weight:600;">{{$bod->position}}</h5>
					</div>
					<div class="text">
						<p>
							{!!$bod->content!!}
						</p>
					</div>



				</div>

			</div>



		</div>
	</section>






@endsection


	<section class="testimonials-section">
		<div class="container">
			<div class="section-title">
				<div class="row">
					<div class="col-md-4 col-sm-5 col-xs-12">
						<div class="title-section text-left">
							<p>Best Review</p>
							<h2>Clients Testimonials</h2>
						</div>
					</div>
					<div class="col-md-8 col-sm-7 col-xs-12 font-20">
						<p>We help interesting companies create and bonding long time relationship and <br> services through long lasting and mutually rewarding relationships.</p>
					</div>
				</div>
			</div>

			<!--Slider-->
			<div class="testimonials-slider column-carousel three-column">

				<!--Slide-->
				<article class="slide-item">
					<div class="quote"><span class="fa fa-quote-right"></span></div>
					<div class="author">
						<div class="img-box">
							<a href="#"><img src="images/resource/t1.png" alt=""></a>
						</div>
						<h4>John Doe</h4>
						<a href="#">
							<p>CEO, HamilTon</p>
						</a>
					</div>

					<div class="slide-text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat vitae neque eu eleifend.Nulla pharetra bibendum magna at hendrerit. Nam sed ligula placerat.</p>
					</div>
				</article>

				<!--Slide-->
				<!-- <article class="slide-item">
					<div class="quote"><span class="fa fa-quote-right"></span></div>
					<div class="author">
						<div class="img-box">
							<a href="#"><img src="images/resource/t2.png" alt=""></a>
						</div>
						<h4>Stephanie Gomez</h4>
						<a href="#">
							<p>CTO, Bikon</p>
						</a>
					</div>

					<div class="slide-text">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ipsum dolor sit amet, consectetur adipiscing elit.</p>
					</div>
				</article> -->

				<!--Slide-->
				<article class="slide-item">
					<div class="quote"><span class="fa fa-quote-right"></span></div>
					<div class="author">
						<div class="img-box">
							<a href="#"><img src="images/resource/t3.png" alt=""></a>
						</div>
						<h4>Clinton Doe</h4>
						<a href="#">
							<p>Project Manager, Kick</p>
						</a>
					</div>

					<div class="slide-text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat vitae neque eu eleifend.Nulla pharetra bibendum magna at hendrerit. Nam sed ligula placerat.</p>
					</div>
				</article>
				<!--Slide-->
				<article class="slide-item">
					<div class="quote"><span class="fa fa-quote-right"></span></div>
					<div class="author">
						<div class="img-box">
							<a href="#"><img src="images/resource/t1.png" alt=""></a>
						</div>
						<h4>Mark Vou</h4>
						<a href="#">
							<p>Team Lead, Shelter</p>
						</a>
					</div>

					<div class="slide-text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat vitae neque eu eleifend.Nulla pharetra bibendum magna at hendrerit. Nam sed ligula placerat.</p>
					</div>
				</article>

				<!--Slide-->
				<article class="slide-item">
					<div class="quote"><span class="fa fa-quote-right"></span></div>
					<div class="author">
						<div class="img-box">
							<a href="#"><img src="images/resource/t2.png" alt=""></a>
						</div>
						<h4>Stephanie Doe</h4>
						<a href="#">
							<p>CTO, Biko</p>
						</a>
					</div>

					<div class="slide-text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat vitae neque eu eleifend.Nulla pharetra bibendum magna at hendrerit. Nam sed ligula placerat.</p>
					</div>
				</article>

				<!--Slide-->
				<article class="slide-item">
					<div class="quote"><span class="fa fa-quote-right"></span></div>
					<div class="author">
						<div class="img-box">
							<a href="#"><img src="images/resource/t3.png" alt=""></a>
						</div>
						<h4>Clinton Josh</h4>
						<a href="#">
							<p>FEA Org.</p>
						</a>
					</div>

					<div class="slide-text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat vitae neque eu eleifend.Nulla pharetra bibendum magna at hendrerit. Nam sed ligula placerat.</p>
					</div>
				</article>
				<!--Slide-->
				<article class="slide-item">
					<div class="quote"><span class="fa fa-quote-right"></span></div>
					<div class="author">
						<div class="img-box">
							<a href="#"><img src="images/resource/t1.png" alt=""></a>
						</div>
						<h4>Mark Doe</h4>
						<a href="#">
							<p>Programmer, CEFALO</p>
						</a>
					</div>

					<div class="slide-text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat vitae neque eu eleifend.Nulla pharetra bibendum magna at hendrerit. Nam sed ligula placerat.</p>
					</div>
				</article>

				<!--Slide-->
				<article class="slide-item">
					<div class="quote"><span class="fa fa-quote-right"></span></div>
					<div class="author">
						<div class="img-box">
							<a href="#"><img src="images/resource/t2.png" alt=""></a>
						</div>
						<h4>Stephanie Hasan</h4>
						<a href="#">
							<p>Relish</p>
						</a>
					</div>

					<div class="slide-text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat vitae neque eu eleifend.Nulla pharetra bibendum magna at hendrerit. Nam sed ligula placerat.</p>
					</div>
				</article>

				<!--Slide-->
				<article class="slide-item">
					<div class="quote"><span class="fa fa-quote-right"></span></div>
					<div class="author">
						<div class="img-box">
							<a href="#"><img src="images/resource/t3.png" alt=""></a>
						</div>
						<h4>Simon Gomez</h4>
						<a href="#">
							<p>NewsCread</p>
						</a>
					</div>

					<div class="slide-text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec feugiat vitae neque eu eleifend.Nulla pharetra bibendum magna at hendrerit. Nam sed ligula placerat.</p>
					</div>
				</article>


			</div>

		</div>
	</section>

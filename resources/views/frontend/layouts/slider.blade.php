<!-- <marquee><p style="color:#fe0000;"><b>TENDER SUBMISSION DATE EXTENSION NOTICE:</b></p></marquee> -->
<section class="rev_slider_wrapper ">
  <div id="slider1" class="rev_slider" data-version="5.0">
    <ul>
      @foreach($slider as $data)
      <li data-transition="slidingoverlayleft">
        <img src="{{asset($data->image->path)}}" alt="" width="1920" height="705" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="2">

        <div class="tp-caption sfl tp-resizeme factory-caption-h1" data-x="left" data-hoffset="0" data-y="top" data-voffset="210" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="500">
                          <!-- {{$data->title}} -->
        </div>
        <div class="tp-caption sfl tp-resizeme factory-caption-p" data-x="left" data-hoffset="0" data-y="top" data-voffset="375" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="500">
                          {{$data->content}}
        </div>
        @if($data->first_button_title)
        <div class="tp-caption sfl tp-resizeme factory-caption-p" data-x="left" data-hoffset="0" data-y="top" data-voffset="475 " data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="500">
          <a href="{{$data->first_button_url}}" class="thm-btn">{{$data->first_button_title}}</a>
        </div>
        @else

        @endif
        @if($data->second_button_title)
        <div class="tp-caption sfl tp-resizeme factory-caption-p" data-x="left" data-hoffset="190" data-y="top" data-voffset="475" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="500">
          <a href="{{$data->second_button_url}}" class="thm-btn thm-tran-bg">{{$data->second_button_title}}</a>
        </div>
        @else
        @endif
      </li>

      @endforeach
    </ul>
  </div>
</section>

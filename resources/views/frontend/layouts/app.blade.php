<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="UTF-8">

      <meta name="_token" content="{!! csrf_token() !!}" />

    <meta name="description" content="#">
    <meta name="keywords" content="quotes on auto insurance, quote insurance, auto insurance online, online insurance, insurance agent, personal insurance, best car insurance,state insurance ">
    <meta name="author" content="iglyphic">
    <link rel="shortcut icon" href="{{asset('images/logo.jpg')}}">
    <title> National Insurance Company Limited-@yield('title')</title>

	<!-- Mobile Specific Meta
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('css/responsive.css') }}">




<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#abc").modal('show');
	});
</script>

@stack('stylesheet')


</head>

<body>

  <!-- Start Loading -->

  <section class="loading-overlay">
  	<div class="Loading-Page">
  		<h1 class="loader">Loading...</h1>
  	</div>
  </section>


  <!-- End Loading  -->

  <div class="boxed_wrapper">


@include('frontend.layouts.header')

@yield('content')

@include('frontend.layouts.footer')



<!--Scroll to top-->
	<div class="scroll-to-top scroll-to-target" data-target=".main-header"><span class="icon flaticon-arrows"></span></div>


</div>


	<!-- jQuery js -->
	<script src="{{asset('assets/jquery/jquery-2.1.4.js')}}"></script>
	<!-- bootstrap js -->
	<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
	<!-- jQuery ui js -->
	<script src="{{asset('assets/jquery-ui-1.11.4/jquery-ui.js')}}"></script>
	<!-- owl carousel js -->
	<script src="{{asset('assets/owl.carousel-2/owl.carousel.min.js')}}"></script>
	<!-- jQuery validation -->
	<script src="{{asset('assets/jquery-validation/dist/jquery.validate.min.js')}}"></script>
	<!-- gmap.js helper -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRvBPo3-t31YFk588DpMYS6EqKf-oGBSI"></script>
	<!-- gmap.js -->
	<script src="{{asset('assets/gmap.js')}}"></script>
	<!-- mixit up -->
	<script src="{{asset('assets/wow.js')}}"></script>
	<script src="{{asset('assets/jquery.mixitup.min.js')}}"></script>
	<script src="{{asset('assets/jquery.fitvids.js')}}"></script>
	<script src="{{asset('assets/bootstrap-select.min.js')}}"></script>

	<!-- revolution slider js -->
	<script src="{{asset('assets/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
	<script src="{{asset('assets/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
	<script src="{{asset('assets/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
	<script src="{{asset('assets/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
	<script src="{{asset('assets/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
	<script src="{{asset('assets/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
	<script src="{{asset('assets/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
	<script src="{{asset('assets/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
	<script src="{{asset('assets/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
	<script src="{{asset('assets/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>

	<script src="{{asset('assets/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>

	<!-- fancy box -->
	<script src="{{asset('assets/fancyapps-fancyBox/source/jquery.fancybox.pack.js')}}"></script>
	<script src="{{asset('assets/Polyglot-Language-Switcher-master/js/jquery.polyglot.language.switcher.js')}}"></script>
	<script src="{{asset('assets/nouislider/nouislider.js')}}"></script>
	<script src="{{asset('assets/bootstrap-touch-spin/jquery.bootstrap-touchspin.js')}}"></script>

	<script src="{{asset('assets/SmoothScroll.js')}}"></script>

	<script src="{{asset('assets/jquery-appear/jquery.appear.js')}}"></script>
	<script src="{{asset('assets/jquery.countTo.js')}}"></script>

	<script type="text/javascript" src="{{asset('assets/counter/jquery.countimator.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/counter/jquery.countimator.wheel.js')}}"></script>

	<!-- theme custom js  -->
	<script id="map-script" src="{{asset('js/default-map.js')}}"></script>
	<script src="{{asset('js/custom.js')}}"></script>

	<script type="text/javascript">
		$(".counter").countimator();
	</script>



</body>
<!-- let's call the following div as the POPUP FRAME -->



</html>

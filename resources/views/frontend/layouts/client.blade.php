
<section class="clients-section">
  <div class="container">
    <div class="client-carousel owl-carousel owl-theme">
      @foreach($client as $data)
      <div class="item">
          <a href="{{$data->title}}" class="abc">
        <img src="{{asset($data->image->path)}}" alt="Insurance Image" />
      </a>
      </div>
      @endforeach
    </div>
  </div>
</section>

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Download extends Model
{
    protected $fillable = [

        'slug',
        'title',
        'path',
        'file_name',
        'size',
        'extension',
        'view',
        'is_published'
    ];

    protected $appends = [
        'public_path'
    ];


    /**
     * The attributes that should be typecast into boolean.
     *
     * @var array
     */
    public static $path = 'downloads/';


    protected $casts = [
        'is_published'    => 'boolean'
    ];


    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getPublicPathAttribute()
    {
        $link = Download::$path . $this->path;
        return $link;
    }


        //for slug
        use HasSlug;

           /**
            * Get the options for generating the slug.
            */
           public function getSlugOptions() : SlugOptions
           {
               return SlugOptions::create()
                   ->generateSlugsFrom('title')
                   ->saveSlugsTo('slug');
           }




}

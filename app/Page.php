<?php

namespace App;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
  protected $fillable=[
    'title',
    'slug',
    'content',
    'meta_description',
    'is_published',

  ];

  protected $casts=[
    'is_published'=>'boolean'
  ];

  use HasSlug;

 /**
  * Get the options for generating the slug.
  */
 public function getSlugOptions() : SlugOptions
 {
     return SlugOptions::create()
         ->generateSlugsFrom('title')
         ->saveSlugsTo('slug');
 }
 /**
  * Get the route key for the model.
  *
  * @return string
  */
 public function getRouteKeyName()
 {
     return 'slug';
 }
 /**
  * @param $query
  * @param bool $type
  * @return mixed
  */
 public function scopePublished($query, $type = true)
 {
     return $query->where('is_published', $type);
 }

 /**
  * @param $query
  * @param bool $type
  * @return mixed
  */
 public function scopePrimary($query, $type = true)
 {
     return $query->where('is_primary', $type);
 }

 /**
  * @return \Illuminate\Database\Eloquent\Relations\MorphOne
  */
 public function image()
 {
     return $this->morphOne(Image::class, 'imageable');
 }

 /**
  * @param array $options
  * @return bool|null|void
  * @throws \Exception
  */
 public function delete(array $options = array())
 {
     if ($this->image)
     {
         $this->image->delete();
       }

     return parent::delete($options);
 }
}

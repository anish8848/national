<?php

namespace App\Http\ViewCreators;

use Illuminate\View\View;

class BackendMenuCreator
{

    /**
     * The user model.
     *
     * @var \App\User;
     */
    protected $user;

    /**
     * Create a new menu bar composer.
     */
    public function __construct()
    {
        $this->user = auth()->user();
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function create(View $view)
    {
        $menu[] = [
            'class' => false,
            'route' => url('/home'),
            'icon'  => 'md md-home',
            'title' => 'Home'
        ];

        array_push($menu, [
            'class' => false,
            'route' => route('slider.index'),
            'icon'  => 'md md-image',
            'title' => 'Slider'
        ]);
        array_push($menu, [
            'class' => false,
            'route' => route('news.index'),
            'icon'  => 'md md-description',
            'title' => 'News'
        ]);

        array_push($menu, [
            'class' => false,
            'route' => route('team.index'),
            'icon'  => 'md md-account-circle',
            'title' => 'Team'
        ]);
        //
        array_push($menu, [
            'class' => false,
            'route' => route('download.index'),
            'icon'  => 'md md-file-download',
            'title' => 'Download'
        ]);
        //
        array_push($menu, [
        'class' => false,
        'route' => route('photo.index'),
        'icon'  => 'md md-image',
        'title' => 'Image'
        ]);
        //
        array_push($menu, [
            'class' => false,
            'route' => route('page.index'),
            'icon'  => 'md md-description',
            'title' => 'Pages'
        ]);
        //
        array_push($menu, [
            'class' => false,
            'route' => route('insurance.index'),
            'icon'  => 'md md-description',
            'title' => 'Insurnace Service'
        ]);
        // array_push($menu, [
        //     'class' => false,
        //     'route' => route('form.index'),
        //     'icon'  => 'md md-assignment-ind',
        //     'title' => 'Vacancy Applied'
        // ]);



        $view->with('allMenu', $menu);
    }
}

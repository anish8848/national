<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\News;
use App\Team;
use App\Download;
use App\Photo;
use App\Page;
use App\Insurance;
use Mail;

class FrontEndController extends Controller
{
  public function home( $slug = null )
  {
    $news   = News::orderBy('created_at','desc')->take(3)->get();
    $slider = Slider::get()->all();
    $popup =  Photo::where('view','popup image')->orderBy('created_at','desc')->take(1)->get();
    $client = Photo::where('view','client')->get();
    $service = Insurance::get()->all();
    return view('index',compact('slider','news','popup','client','service'));
  }

  public function news($slug = null)
  {
    $new = News::whereSlug($slug)->first();
    $related_news = News::whereNotIn('id',[$new->id])->orderBy('created_at','desc')->take(5)->get();
    return view('frontend.news.index',compact('new','related_news'));
  }

  public function newsmenu()
  {
    $new = News::get()->all();
    return view('frontend.news.news-menu',compact('new'));
  }

  public function bod()
  {
    $team = Team::where('view','board-of-director')->get();
    return view('frontend.layouts.partials.bod',compact('team'));
  }
  public function corporate_management()
  {
    $corporate = Team::where('view','corporate-management')->get();
    return view('frontend.layouts.partials.corporate-management',compact('corporate'));
  }
  public function finance()
  {
    $financial_report = Download::where('view','financial reports')->get();

    return view('frontend.download.financial-report',compact('financial_report'));
  }

  public function claim()
  {
    $claim = Download::where('view','claim intimation form')->get();
    return view('frontend.download.claim-intimation',compact('claim'));
  }

  public function kyc()
  {
    $kyc = Download::where('view','kyc form')->get();
    return view('frontend.download.kyc',compact('kyc'));
  }

  public function propose()
  {
    $proposal_forms = Download::where('view','proposal forms')->get();
    return view('frontend.download.proposal-forms',compact('proposal_forms'));
  }

  public function branch()
  {
    return view('frontend.layouts.partials.branches');
  }


  public function insurances($slug = null)
  {
    $insurance = Insurance::whereSlug($slug)->first();
    $tender = Download::where('view','others')->get();

    return view('frontend.layouts.partials.insurance',compact('insurance','tender'));
  }




  public function about()
  {
    $company = Page::whereSlug('company-profile')->first();

    $mission = Page::whereSlug('mission')->first();
    $vision = Page::whereSlug('vision')->first();

    return view('frontend.layouts.partials.about-us',compact('company','mission','vision'));
  }

  public function company_profile_world()
  {
    $company_profile = Page::whereSlug('company-profile-world')->first();
    $vision = Page::whereSlug('vision')->first();
    $mission = Page::whereSlug('mission')->first();
    $products = Page::whereSlug('products-of-national-insurance')->first();
    $corporate = Page::whereSlug('corporate-social-responsibility')->first();
    $awards    = Page::whereSlug('awards-achievements')->first();
    return view('frontend.layouts.partials.company-profile-world',compact('vision','company_profile','mission','products','corporate','awards'));
  }
  public function key()
  {
    $key = Page::whereSlug('key-person')->first();
    return view('frontend.layouts.partials.key-person',compact('key'));
  }


  public function profile( $slug = null)
  {
    $bod = Team::whereSlug($slug)->first();
    return view('frontend.layouts.partials.profile-details',compact('bod'));
  }


  public function getContact()
  {

    return view('frontend.layouts.partials.contact');
  }
  public function PostContact(Request $request )
  {
    $this->validate($request,[
      'name'=>'required',
      'email'=>'required|email',
      'subject  '=>'required|max:200',
      'message'=>'required'

    ]);
    $data = array(
      'name'  =>$request->name,
      'email' => $request->email,
      'subject' => $request->subject,
      'bodyMessage' => $request->message
    )   ;


    Mail::send('emails.contact', $data,function($message) use($data){
      $message->from($data['email']);
      $message->to('badevilanish@gmail.com');
      $message->subject($data['subject']);



    });
  }
}

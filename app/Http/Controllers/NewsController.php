<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreNews;
use App\Http\Requests\UpdateNews;
use Illuminate\Support\Facades\DB;
use App\Image;
use App\News;

class NewsController extends Controller
{
    public function index()
    {
      $news = News::latest()->get();
      return view('backend.news.index',compact('news'));
    }

    public function create()
    {
      return view('backend.news.create');
    }

    public function store(StoreNews $request)
    {
      DB::transaction(function () use ($request){
        $data = $request->data();
        $new = News::create($data);
        $this->uploadRequestImage($request,$new);
      });
        return redirect()->route('news.index')->withsuccess(trans('news has been successfully created',[ 'entity' => 'news']));
    }


      public function show(News $new)
      {
        return view($new->view,compact('news'));
      }

      public function edit(News $new)
      {
        return view('backend.news.edit',compact('new'));
      }

      public function update(UpdateNews $request,News $new)
      {
        DB::transaction(function () use($request,$new)
        {
          $new->update($request->data());

          $this->uploadRequestImage($request,$new);
        });
        return redirect()->route('news.index')->withsuccess(trans('news has been successfully updated',['entity'=>'news']));
      }

      public function delete(news $new)
      {
           $new->delete();

            return back()->withsuccess(trans('news has been deleted successfully', ['entity'=>'news']));
      }
}

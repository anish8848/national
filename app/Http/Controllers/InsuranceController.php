<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreInsurance;
use App\Http\Requests\UpdateInsurance;
use Illuminate\Support\Facades\DB;
use App\Image;
use App\Insurance;

class InsuranceController extends Controller
{
  public function index()
  {
    $insurances = Insurance::latest()->get();
    return view('backend.insurance.index',compact('insurances'));
  }

  public function create()
  {
    return view('backend.insurance.create');
  }

  public function store(StoreInsurance $request)
  {
    DB::transaction( function () use ($request)
    {
      $data = $request->data();
      $insurance = Insurance::create($data);
      $this->uploadRequestImage($request,$insurance);
    });
    return redirect()->route('insurance.index')->withsuccess(trans('insurance pages has been successfully created',[ 'entity' => 'insurance']));
  }

  public function show(Insurance $insurance)
  {
    return view($insurance->view,compact('insurances'));
  }

  public function edit(Insurance $insurance)
  {
    return view('backend.insurance.edit',compact('insurance'));
  }

  public function update(UpdateInsurance $request,Insurance $insurance)
  {
    DB::transaction(function () use($request,$insurance)
    {
      $insurance->update($request->data());

      $this->uploadRequestImage($request,$insurance);
    });
    return redirect()->route('insurance.index')->withsuccess(trans('insurance pages has been successfully updated',['entity'=>'insurance']));
  }

  public function delete(insurance $insurance)
  {
       $insurance->delete();

        return back()->withsuccess(trans('insurance has been deleted successfully', ['entity'=>'insurance']));
  }

}

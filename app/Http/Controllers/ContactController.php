<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactController extends Controller
{
  public function index()
  {
  
    return view('frontend.layouts.partials.contact');
  }

  public function post(Request $request){
    $this->validate($request,[
      'name'=>'required',
      'email'=>'required|email',
      'subject'=>'required|max:200',
      'message'=>'required'
    ]);

    $data = array(
      'name'  =>$request->name,
      'email' => $request->email,
      'subject' => $request->subject,
      'bodyMessage' => $request->message
    );


     Mail::send('emails.contact', $data,function($message) use($data){
        $message->from($data['email']);
        $message->to('badevilanish@gmail.com');
        $message->subject($data['subject']);



      });
    return redirect()->back()->with('message', 'Your form has been successfully submitted!!!');
  }
}
